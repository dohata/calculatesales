package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST= "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_SEQUENCE = "売上ファイル名が連番になっていません";
	private static final String FILE_OVER_NUMBER = "合計金額が10桁を超えました";
	private static final String FILE_FORMAT = "のフォーマットが不正です";
	private static final String FILE_CORD = "の支店コードが不正です";
	private static final String FILE_COMMODITY_FORMAT ="商品定義フォーマットが不正です";
	private static final String FILE_NOT_EXIST_COMMODITY ="商品定義ファイルが存在しません";
	private static final String FILE_COMMODITY_CORD = "の商品コードが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		if(args.length != 1) {//エラー処理
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySalse = new HashMap<>();
		//正規表現の引数支店バージョン
		String regexpBranch = "^[0-9]{3}$";
		//正規表現の引数商品バージョン
		String regexpCommodity = "^[0-9a-zA-Z]{8}$";

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, regexpBranch, FILE_INVALID_FORMAT, FILE_NOT_EXIST)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySalse, regexpCommodity, FILE_COMMODITY_FORMAT, FILE_NOT_EXIST_COMMODITY)) {
			return;
		}

        File[] files = new File(args[0]).listFiles();
        List<File> rcdFiles = new ArrayList<>();
        for(int i = 0; i < files.length ; i++) {
        		//対象がファイルであり、「数字8桁.rcd」なのか判定
        	if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$" )) {
             		rcdFiles.add(files[i]);
        	}
        }
        Collections.sort(rcdFiles);
    	for(int i = 0; i < rcdFiles.size() - 1; i++ ) {
    		int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
    		int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
    		if((latter - former) != 1) {//エラー処理
    			System.out.println(FILE_SEQUENCE);
    			return;
    		}
    	}

        //売上ファイルの読み込み
        for(int i = 0; i < rcdFiles.size(); i++) {
        	BufferedReader br = null;

        	try {
	        	FileReader fr = new FileReader(rcdFiles.get(i));
	        	br = new BufferedReader(fr);
	        	String loading;
	         	List<String> line = new ArrayList<>();
	        	while((loading = br.readLine()) != null) {
	        		line.add(loading);//
	        	}
	        	if(line.size() != 3) {//エラー処理
	        		System.out.println(rcdFiles.get(i).getName() + FILE_FORMAT);
	        		return ;
	        	}
	        	 String branchCode = line.get(0);//支店コードの入れ物

	        	if(!branchNames.containsKey(branchCode)) {//支店コードエラー処理
	        		System.out.println(rcdFiles.get(i).getName() + FILE_CORD);
	        		return;
	        	}
	        	String commodity = line.get(1);//商品コードの入れ物

	        	if(!commodityNames.containsKey(commodity)) {//商品コードエラー処理
	        		System.out.println(rcdFiles.get(i).getName() + FILE_COMMODITY_CORD);
	        		return;
	        	}
        		String salse = line.get(2);//売上金額の入れ物

	        	if(!salse.matches("^[0-9]+$")) {//売上金額が数字なのか確認するエラー処理
	        		System.out.println(UNKNOWN_ERROR);
	    			return ;
	    		}
        		long fileSale = Long.parseLong(salse);//longに変換してる
        		Long saleAmount = branchSales.get(branchCode) + fileSale;            //支店マップの売上金額と合算
        		Long saleAmountCommodity = commoditySalse.get(commodity) + fileSale;//商品マップの売上金額と合算

	        	if(saleAmount >= 100000000L || saleAmountCommodity >= 100000000L)  {//桁数エラー処理
	        		System.out.println(FILE_OVER_NUMBER);
	        		return;
	        	}
	        	branchSales.put(branchCode,saleAmount);           //合算した売上を支店マップに記録
	        	commoditySalse.put(commodity,saleAmountCommodity);//合算した売上を商品マップに記録

        	} catch(IOException e) {
    			System.out.println(UNKNOWN_ERROR);
    			return ;
    		} finally {
    			// ファイルを開いている場合
    			if(br != null) {
    				try {
    					// ファイルを閉じる
    					br.close();
    				} catch(IOException e) {
    					System.out.println(UNKNOWN_ERROR);
    					return ;
    				}
    			}
    		}

        }
        		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
				// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySalse)) {
			return;
		}


	}

	/**
	 * 支店定義ファイル読み込み処理
	 * 商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> salse, String regexp, String error, String fileError) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//ファイルの存在をチェック
			if(!file.exists()) {
				System.out.println(fileError);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if((items.length != 2) || (!items[0].matches(regexp))) {
					System.out.println(error);
					return false;
				}
				names.put(items[0], items[1]);
				salse.put(items[0],0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> salse) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key:  names.keySet()) {
				bw.write(key + "," + names.get(key) +"," + salse.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
